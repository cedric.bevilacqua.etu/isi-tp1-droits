import os

ruid, euid, suid = os.getresuid()
rgid, egid, sgid = os.getresgid()

print("RUID : " + str(ruid))
print("EUID : " + str(euid))
print("SUID : " + str(suid))
print("RGID : " + str(rgid))
print("EGID : " + str(egid))
print("SGID : " + str(sgid))
# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- BEVILACQUA, Cédric, cedric.bevilacqua.etu@univ-lille.fr
- ALTINKAYNAK, Sema, semae.altinkaynak.etu@univ-lille.fr

## Question 1

Le processus a été lancé par toto et a donc les mêmes UID et GID. Le fichier que le processus essaie de lire appartient à toto, donc l’EUID du processus correspond au propriétaire du fichier. C’est donc le premier triplet qui va s’appliquer et donc seul le droit de lecture sera présent. Le processus ne pourra donc pas écrire. Pour cela, il faudrait que toto modifie les droits qu’il a sur son propre fichier avec un CHMOD par exemple.

## Question 2

Pour un répertoire, le caractère x autorise l’accès au répertoire et son ouverture.
Lorsque toto tente d’accéder au répertoire, il n’est pas le créateur et donc c’est le 2ème triplet qui va s’appliquer, celui lié au groupe car il fait partie du même groupe ubuntu que l’utilisateur ubuntu. Mais comme le droit d’exécution aura été supprimé sur ce répertoire, il ne pourra pas l’ouvrir.
Lorsque toto tente de lister le contenu du dossier, il peut voir l’intérieur de celui-ci mais les droits de chaque fichier et leur taille sont cachés. De même, aucun fichier ne peut être ouvert et aucune modification ne peut être réalisée. Seul l’index du dossier est accessible.

Voici les droits accordées pour le fichier mydir :
drwxrw-r-- 2 ubuntu ubuntu 4096 Jan 12 16:23 mydir

ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .  
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt

## Question 3

Toutes les valeurs EUID, EGID, RUID et RGID sont à 1001. Le fichier n’a pas pu être ouvert et on a un Segmentation fault à la fin.
Après avoir activé le flag set-user-id, le processus parvient à ouvrir le fichier. Les EGID, RUID et RGID sont toujours à 1001 mais le EUID est passé à 1000, ce qui correspond à l’UID de l’utilisateur ubuntu.

Sur Ubuntu : 
ubuntu@machine:~$ ./quest3
EUID : 1000
EGID : 1000
RUID : 1000
RGID : 1000

Sur toto : 
toto@machine:/home/ubuntu$ ./quest3 
EUID : 1001
EGID : 1001
RUID : 1001
RGID : 1001
Segmentation fault (core dumped)



ubuntu@machine:~$ ls -al
total 80
drwxr-xr-x 7 ubuntu ubuntu  4096 Jan 19 15:50 .
drwxr-xr-x 4 root   root    4096 Jan 12 15:50 ..
-rw------- 1 ubuntu ubuntu   607 Jan 12 17:27 .bash_history
-rw-r--r-- 1 ubuntu ubuntu   220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 ubuntu ubuntu  3771 Feb 25  2020 .bashrc
drwx------ 2 ubuntu ubuntu  4096 Jan 12 15:31 .cache
drwxrwxr-x 3 ubuntu ubuntu  4096 Jan 19 15:31 .local
-rw-r--r-- 1 ubuntu ubuntu   807 Feb 25  2020 .profile
drwx------ 2 ubuntu ubuntu  4096 Jan 12 15:23 .ssh
-rw-r--r-- 1 ubuntu ubuntu     0 Jan 12 15:35 .sudo_as_admin_successful
drwxrwxr-x 2 ubuntu ubuntu  4096 Jan 12 16:19 data.txt
-rw-rw-r-- 1 ubuntu ubuntu     5 Jan 19 15:33 es
drwxrw-r-- 2 ubuntu ubuntu  4096 Jan 19 15:51 mydir
-rw-rw-r-- 1 ubuntu ubuntu     0 Jan 12 15:55 myfile.txt
-rw------- 1 root   root      88 Jan 12 17:27 nano.save
-rwsrwxr-x 1 ubuntu ubuntu 17056 Jan 19 15:50 quest3
-rw-rw-r-- 1 ubuntu ubuntu   558 Jan 19 15:37 quest3.c
-rw-rw-r-- 1 ubuntu ubuntu  2672 Jan 19 15:49 quest3.o

Après le set-user-id : 
toto@machine:/home/ubuntu$ ./quest3 
EUID : 1000
EGID : 1001
RUID : 1001
RGID : 1001
hello world

## Question 4

Toutes les valeurs des RUID, EUID, SUID, RGIS, EGID et SGID sont à 1001. Contrairement à la question précédente, rien n’a changé lors de la définition du set-user-id. Cela est dû au fait que Python n’est pas un exécutable mais un script qui s’exécute dans un processus Python séparé du fichier. Changer le set-user-id sur le fichier du script ne changera donc en aucun cas les ID du processus car le script est simplement importé par un autre processus.

toto@machine:/home/ubuntu$ python3 script.py 
Real effective id of the current process: 1001
Real effective id of the current process: 1001

## Question 5

La commande chfn permet de modifier les attributs d’un utilisateur dans le fichier /etc/passwd.
Pour qu’un utilisateur puisse changer ces attributs, il utilise une commande qui est un exécutable possédant un set-user-id qui lui donne des droits permettant l’accès au fichier protégé passwd. Ainsi, l’utilisateur peut se servir de cet exécutable pour lui demander d’effectuer des modifications sur ce fichier, l’exécutable qui aura donc les droits pourra le faire à sa place.L'utilisateur root a défini le set-user-id c'est pourquoi le processus chfn a des privilèges plus elevée et donc toto peut modifié ces informations personnelles.

Contenu avant modification des informations de toto : 

sshd:x:109:65534::/run/sshd:/usr/sbin/nologin
landscape:x:110:115::/var/lib/landscape:/usr/sbin/nologin
pollinate:x:111:1::/var/cache/pollinate:/bin/false
systemd-coredump:x:999:999:systemd Core Dumper:/:/usr/sbin/nologin
ubuntu:x:1000:1000:Ubuntu,,,:/home/ubuntu:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
toto:x:1001:1001::/home/toto:/bin/bash

On voit ici que le droit S est présent pour les deux utilisateurs : 

ubuntu@machine:~$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

root@machine:/home/ubuntu# ls -al /usr/bin/chfn 
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

Après modification des informations de toto : 

ubuntu:x:1000:1000:Ubuntu,,,:/home/ubuntu:/bin/bash
lxd:x:998:100::/var/snap/lxd/common/lxd:/bin/false
toto:x:1001:1001:,4,023547851,51544244:/home/toto:/bin/bash

## Question 6

Les mots de passe de l’utilisateur sont stockés dans le fichier /etc/shadow sous la forme de hash des mots de passe.
En effet, le fichier passwd est accessible en lecture par tout le monde, on peut donc aller lire les informations sur tous les utilisateurs. Ce n’est pas le cas du fichier shadow qui ne peut pas être lu autrement que par root ou au travers de commandes spécifiques qui ont un set-user-Id qui leur permet d’effectuer des opérations sur ce fichier.

ubuntu@machine:~$ id
uid=1000(ubuntu) gid=1000(ubuntu) groups=1000(ubuntu),4(adm),20(dialout),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),117(netdev),118(lxd)
ubuntu@machine:~$ su root
Password: 
root@machine:/home/ubuntu# id
uid=0(root) gid=0(root) groups=0(root)
root@machine:/home/ubuntu# 

ubuntu@machine:~$ ls -al /etc/shadow
-rw-r----- 1 root shadow 1261 Jan 19 16:48 /etc/shadow

Nous pouvons donc voir que comme ubuntu ne fait pas parti du groupe de root il ne peut visualiser le contenu de ce fichier.

## Question 7

Les scripts bash sont présent dans le repertoire *question7*.
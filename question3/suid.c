#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[]) {
    printf("EUID : %d", geteuid());
    printf("\n");
    printf("EGID : %d", getegid());
    printf("\n");
    printf("RUID : %d", getuid());
    printf("\n");
    printf("RGID : %d", getgid());
    printf("\n");

    FILE* f = NULL;
    f = fopen("mydir/data.txt", "r");
    int caractereActuel = 0;
    do {
        caractereActuel = fgetc(f);
        printf("%c", caractereActuel);
    } while(caractereActuel != EOF);
    fclose(f);
}
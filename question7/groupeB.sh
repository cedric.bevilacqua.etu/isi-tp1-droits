adduser lambda_b
addgroup groupe_b
usermod -a -G  groupe_b lambda_b
chgrp groupe_b dir_b
chmod 771 dir_b
chmod +t dir_b
sudo su lambda_b

echo "Test du point de vue de lambda_b"
echo "Test 1 : Creation d'un fichier dans dir_b et ecriture dans le fichier"
echo "Hello" > dir_b/testb.txt
echo "Test 2 : Ajout de lignes dans testb.txt" > dir_b/testb.txt
cat dir_b/testb.txt
echo "Test 3 : Création d'un fichier dans dir_b"
touch dir_b/nouveaufichier.txt
echo "Test 4 : Création d'un repertoire dans dir_b"
mkdir dir_b/nouveaurepertoire/
echo "Modification d'un fichier dans dir_b"
echo "Test 5 : j'ajoute des lignes dans fichier2.txt" > dir_b/fichier2.txt
cat dir_b/fichier2.txt
echo "Test 6 : Lecture d'un fichier dans dir_a"
cat dir_a/testa.txt
echo "Test 7 : Modification impossible dans dir_a depuis lambda_b"
echo "j'ajoute des lignes dans testa.txt" > dir_a/testa.txt
cat dir_a/testa.txt
echo "Test 8 : Supprimer un fichier de dir_a en tant que lambda_b"
rm dir_a/nouveaufichier.txt
echo "Test 9 : Creer un fichier dans dir_a en tant que lambda_b"
touch dir_a/nouveaufichier.txt
echo "Test 10 : Lecture d'un fichier dans dir_c"
cat dir_c/testc.txt
echo "Test 11 : Ajout de lignes dans testc.txt" > dir_c/testc.txt
cat dir_c/testc.txt
echo "Test 12 : Renommer un fichier de dir_c en tant que lambda_b"
mv dir_c/testc dir_c/nouveaufichier.txt
echo "Test 13 : Supression d'un fichier de dir_c en tant que lambda_b"
rm dir_c/nouveaufichier.txt
echo "Test 14 : Création d'un fichier dans dir_c en tant que lambda_b"
touch dir_c/nouveaufichier.txt




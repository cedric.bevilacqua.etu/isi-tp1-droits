adduser lambda_a
addgroup groupe_a
usermod -a -G  groupe_a lambda_a
chgrp groupe_a dir_a
chmod 771 dir_a
chmod +t dir_a

sudo su lambda_a
echo "Test du point de vue de lambda_a"
echo "Hello" > dir_a/testa.txt
echo "Test 1 : Lecture d'un répertoire"
cat dir_a/folderA
echo "Test 2 : Ajout de ligne dans testa.txt" > dir_a/testa.txt
cat dir_a/testa.txt
echo "Test 3 : Création  d'un fichier dans dir_a"
touch dir_a/nouveaufichier.txt
echo "Test 4 : Création  d'un repertoire dans dir_a"
mkdir dir_a/nouveaurepertoire/
echo "Modification d'un fichier crée par un autre utilisateur dans dir_a"
echo "Test 5 : j'ajoute des lignes dans fichier2.txt" > dir_a/fichier2.txt
cat dir_a/fichier2.txt
echo "Test 6 : Lecture d'un fichier dans dir_c"
cat dir_c/testc.txt
echo "Test 7 : Modification impossible dans dir_c depuis l'utilisateur lambda_a"
echo "Ajout de lignes dans testc.txt" > dir_c/testc.txt
cat dir_c/testc.txt
echo "Test 8 : Renommer un fichier de dir_c en tant que lambda_a"
mv dir_c/testc dir_c/nouveaufichier.txt
echo "Test 9 : Suppresion d'un fichier de dir_c en tant que lambda_a"
rm dir_c/nouveaufichier.txt
echo "Test 10: Création d'un fichier dans dir_c en tant que lambda_a"
touch dir_c/nouveaufichier.txt
echo "Test 11 : lecture d'un fichier dans dir_b"
cat dir_b/testb.txt
echo "Test 12 : Modification impossible dans dir b "
echo "j'ajoute des lignes dans testb.txt" > dir_b/testb.txt
cat dir_b/testb.txt
echo "Test 13 : Supprimer un fichier de dir_b "
rm dir_b/nouveaufichier.txt
echo "Test 14 : Creer un fichier dans dir_b "
touch dir_b/nouveaufichier.txt





